﻿using System;
using System.Data;
using System.IO;
using System.Web.Services;
using System.Xml.Serialization;
using System.Data.OracleClient;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class Service : System.Web.Services.WebService
{
    OracleAcess oracleacess = new OracleAcess("CRM_ConnectionString");
    string SQL = "";
    DataSet Ds = new DataSet();
    DataTable dt = new DataTable();

    public Service()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    //*********************************************************************** ( Users $ Password  ) ****************************
    #region " User Settings "

    [WebMethod]
    public bool Check_Loged_User (string User_Name , string Password)
    {
        bool ret_val = false ;

        try
        {
            string INC_PASS = Password.Insert(0, "W") ;
            string New_PASS = INC_PASS.Insert(4, "YM");
            SQL = "SELECT PASS FROM USERS WHERE USER_NAME = '"+ User_Name.ToUpper().ToString() +"' AND STATUS = 'N' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (Pass.ToString().ToUpper().Trim() == New_PASS.ToUpper().Trim())
            {
                ret_val = true;
            }
            else
            {
                ret_val = false;
            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_Loged_User >> to check logged user and password");
            ret_val = false;
        }

        return ret_val;
    }
     
    [WebMethod]
    public string Get_User_Name(string User_Name)
    {
        string ret_val = "";

        try
        {
            SQL = " select arabic_name from users where user_name = '" + User_Name.ToUpper().ToString() + "' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_User_Name >> To get User Name");
        }

        return ret_val;
    }

    [WebMethod]
    public string Check_Authorization( string User_Name , string form_name )
    {
        string ret_val = "" ;

        try
        {   
            SQL = " select SV_priv('"+form_name+"' , '"+User_Name+"') from dual ";
            //SV_R001
            //SV_VISITS
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_Authorization >> To check user privalages");
        }

        return ret_val ;
    }

    [WebMethod]
    public string Get_User_Type(string User_Name)
    {
        string ret_val = "";
        try
        {
            SQL = " SELECT USER_TYPE FROM USERS WHERE UPPER(USER_NAME) =UPPER('" + User_Name + "')";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_User_Type >> To check user type");
        }

        return ret_val;
    }

    #endregion

    //*********************************************************************** ( Internal Func     ) *****************************
    #region " Internal Functions "

    public void LogError(string msg, string functionName)
    {
        StreamWriter sr = new StreamWriter(@"C:\Inetpub\wwwroot\VISITES_WS\LogError.txt", true);
        string txt = "Time: " + DateTime.Now.ToString() + "\r\n";
        txt += "Function Name: " + functionName + "\r\n";
        txt += "Error Message: " + msg + "\r\n";
        txt += "================================= *** =================================\r\n";

        sr.WriteLine(txt);
        sr.Close();
    }

    #endregion

    //*********************************************************************** ( Planned and Actual visites in Maps ) ************
    #region "  Planned and Actual visites  "

    
    [WebMethod]
    public string Get_REP_CODE(string User_Name)
    {
        string ret_val = "";

        try
        {
            SQL = "  select code from Sv_Reps where rel_USER_NAME = '"+User_Name+"'  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_REP_CODE >> To Rep Code ");
        }

        return ret_val;
    }


    [WebMethod]
    public DataSet Bind_REP_DDL()
    {
        try
        {
            SQL = " select CODE , code || '  ' || NAME as NAME FROM SV_REPS WHERE VIEW_FLAG = 1 ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_REP_DDL >> Fill Rep to DDL");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Get_Rep_Jobs_Anls(string rep_code )
    {
        try
        {
            SQL = " select rep_code , rep_name , visits_count , distinct_shops , avg_shops , days_count , total_work_hours , avg_work_hours from SV_REP_JOB_ANLS Where ( rep_code = '" + rep_code + "' or  '" + rep_code + "' = '0'  )  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Rep_Jobs_Anls  >> Fill Rep to DDL");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_Map_Legend(string type, string manager_ID )
    {
        try
        {
            if (type == "SUPPER")
            { SQL = "    select code , name , color_value ,  SV_REP_DATA(code, 1 ) as covered_area ,   SV_REP_DATA(code, 2 ) as shops_count , SV_REP_DATA(code, 3 ) as viewing_count      from sv_reps  where rep_class = 'مدير منطقة' "; }
            else
                if (type == "MANAGER")
                { SQL = "  select code , name , color_value ,  SV_REP_DATA(code, 1 ) as covered_area ,   SV_REP_DATA(code, 2 ) as shops_count , SV_REP_DATA(code, 3 ) as viewing_count   from sv_reps  where ( mang_code = '" + manager_ID + "' or '" + manager_ID + "' = '0' )   "; }
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_Map_Legend >> Fill Map Legend ");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_Sal_Managers_DDL()
    {
        try
        {
            SQL = " select Code as area_super_code , Name as area_super_name , color_value as super_color  from SAL_AREA_MANGERS  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_Sal_Managers_DDL >> Fill sales area managers to DDL");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_Supervisor_DDL()
    {
        try
        {
            SQL = " select Code , Name from SAL_AREA_SUPERS  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_Supervisor_DDL >> Fill Rep to DDL");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_SHOPS_DDL()
    {
        try
        {
            SQL = " select SAL_CODE AS CODE , SHOP_NAME || ' *** ' || SUB_AREA || ' *** ' || RESP_NAME   AS NAME  FROM SV_SHOPS Order by SHOP_NAME ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_SHOPS_DDL >> Fill Rep to DDL");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_SHOPS_Per_Plan(string rep_code , string ddate)
    {
        try
        {
            SQL = "  select shop_code , shop_name , longtude , latitude , planned_visit , visit_status , rep_code from SV_SHOP_VISITS_V where (longtude is not null or TO_NUMBER(longtude)>0 ) and visit_date = '" + ddate + "' and rep_code = '" + rep_code + "'  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_SHOPS_Per_Plan >> Fill shops map");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Get_Shops_Center_Points(string rep_code, string ddate)
    {
        try
        {
            SQL = "  Select to_char(AVG( longtude )) , to_char(AVG( latitude ))  from SV_SHOP_VISITS_V where  longtude is not null and visit_date = '" + ddate + "' and rep_code = '" + rep_code + "'  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, " << Get_Shops_Center_Points >> ");
        }

        return Ds;
    }

    [WebMethod]
    public DataSet Get_Shops_by_Area( string manager_ID  , string shop_class , string rec_type  )
    {
        try
        {
            SQL = "  select  shop_code , shop_name , round(longtude,6) as longtude ,  round(latitude,6) as latitude , area_mang_code , area_super_code ,  manager_color , super_color , in_plan  from SV_SHOP_RESP where ( area_mang_code = '" + manager_ID + "' )  AND  ( TRAD_CLASS IN (" + shop_class + ") OR TRAD_CLASS IS NULL  ) and  ( RECORD_TYPE IN (" + rec_type + ") ) ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, " << Get_Shops_by_Area >> ");
        }

        return Ds;
    }

    [WebMethod]
    public DataSet Get_Shops_for_all()
    {
        try
        {
            SQL = "  select  shop_code , shop_name , longtude , latitude , area_mang_code , area_super_code ,  manager_color , super_color from SV_SHOP_RESP  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, " << Get_Shops_for_all >> ");
        }

        return Ds;
    }

    [WebMethod]
    public DataSet Get_All_Data()
    {
        try
        {
            SQL = "  select  shop_name , longtude , latitude  from SV_SHOP_RESP  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, " << Get_All_Data >> ");
        }

        return Ds;
    }

    [WebMethod]
    public DataSet Get_Cluster_Supervisor_Shops_by_Manager(string manager_ID, string supervisor_ID, string shp_clas, string record_type)
    {
        try
        {
            SQL = "  select  SHOP_CODE , SHOP_NAME ,  LONGTUDE,  LATITUDE , area_mang_code , area_super_code ,  manager_color , super_color , in_plan from SV_SHOP_RESP where ( area_mang_code = '" + manager_ID + "' OR  '" + manager_ID + "' = '0' )  AND ( area_super_code = '" + supervisor_ID + "' or '" + supervisor_ID + "' = '0'  ) AND ( trad_class in ( " + shp_clas + " ) OR trad_class IS NULL ) and ( RECORD_TYPE in (  " + record_type + " ) ) ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, " << Get_Cluster_Supervisor_Shops_by_Manager >> ");
        }

        return Ds;
    }

    [WebMethod]
    public DataSet Get_Supervisor_Shops_by_Manager(string manager_ID , string supervisor_ID , string shp_clas , string record_type )
    {
        try
        {
            SQL = "  select  shop_code , shop_name , longtude , latitude , area_mang_code , area_super_code ,  manager_color , super_color , in_plan from SV_SHOP_RESP where ( area_mang_code = '" + manager_ID + "' )  AND ( area_super_code = '" + supervisor_ID + "' or '" + supervisor_ID + "' = '0'  ) AND ( trad_class in ( " + shp_clas + " ) OR trad_class IS NULL ) and ( RECORD_TYPE in (  " + record_type + " ) ) ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, " << Get_Supervisor_Shops_by_Manager >> ");
        }

        return Ds;
    }

    [WebMethod]
    public DataSet Get_Supers_Shops_by_Area(string manager_ID , string shp_class , string record )
    {
        try
        {
            SQL = "  select  Distinct area_super_code ,  manager_color , super_color , area_super_name   from SV_SHOP_RESP where ( area_mang_code = '" + manager_ID + "' ) and  ( trad_class in ( " + shp_class + " ) OR trad_class is null  ) and   RECORD_TYPE in ( " + record + " )  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, " << Get_Supers_Shops_by_Area >> ");
        }

        return Ds;
    }

    [WebMethod]
    public DataSet Get_Center_Shops_by_Area(string manager_ID, string shp_class, string record )
    {
        try
        {
            SQL = "  select  to_char(AVG(longtude)) as longg , to_char(AVG(latitude))  as latt from SV_SHOP_RESP where ( area_mang_code = '" + manager_ID + "'  )  and  trad_class in ( " + shp_class + " ) and  RECORD_TYPE in ( " + record + " )  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, " << Get_Shops_by_Area >> ");
        }

        return Ds;
    }

    [WebMethod]
    public DataSet Get_Rep_Location(string Rep_ID ,  string day_date , string low_perc )
    {
        try
        {
            // 10 = 10%
            // 5  = 50 %
            // 2 =  80%
            SQL = "  select * from ( select l.* , rownum r_num from sv_rep_location l where trunc(loc_time) = '" + day_date + "' and Rep_Code = '" + Rep_ID + "' order by loc_time) where floor(r_num/" + low_perc + ") = r_num/" + low_perc + "  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, " << Get_Rep_Location >> ");
        }

        return Ds;
    }

    [WebMethod]
    public DataSet Get_Center_Rep_Location(string Rep_ID, string day_date, string low_perc)
    {
        try
        {
            SQL = "  select  to_char(avg(latitude)) ,  to_char(avg(longtude)) from ( select l.* , rownum r_num from sv_rep_location l where trunc(loc_time) = '" + day_date + "' and Rep_Code = '" + Rep_ID + "' order by loc_time) where floor(r_num/" + low_perc + ") = r_num/" + low_perc + "  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, " << Get_Center_Rep_Location >> ");
        }

        return Ds;
    }

    [WebMethod]
    public DataSet Get_Center_Shops_for_all()
    {
        try
        {
           // SQL = "  select  to_char(AVG(longtude)) as longg , to_char(AVG(latitude))  as latt from SV_SHOP_RESP  ";
            SQL = "   select  round( to_char(AVG(longtude)),6) as longg , round(to_char(AVG(latitude)),6)  as latt from SV_SHOP_RESP  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, " << Get_Shops_by_Area >> ");
        }

        return Ds;
    }

    [WebMethod]
    public DataSet Bind_SHOPS_DDL_By_Resp (string RESP )
    {
        try
        {
            SQL = " select SAL_CODE AS CODE , SHOP_NAME || ' *** ' || SUB_AREA || ' *** ' || RESP_NAME   AS NAME   FROM SV_SHOPS WHERE  RESP_NAME LIKE '%" + RESP + "%' Order by SHOP_NAME  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_SHOPS_DDL_By_Resp >> Fill Rep to DDL");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_SHOPS_DDL_By_Mobile(string Mobile )
    {
        try
        {
            SQL = " select SAL_CODE AS CODE , SHOP_NAME || ' *** ' || SUB_AREA || ' *** ' || RESP_NAME   AS NAME  FROM SV_SHOPS WHERE  Mobile = '" + Mobile + "' Order by SHOP_NAME  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_SHOPS_DDL_By_Mobile >> Fill Rep to DDL");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_SHOPS_DDL_By_Area (string Area)
    {
        try
        {
            SQL = " select SAL_CODE AS CODE , SHOP_NAME || ' *** ' || SUB_AREA || ' *** ' || RESP_NAME   AS NAME   FROM SV_SHOPS WHERE  SUB_AREA LIKE '%" + Area + "%' Order by SHOP_NAME  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_SHOPS_DDL_By_Area >> Fill Rep to DDL");
        }
        return Ds;
    }


    #endregion

    //*********************************************************************** ( Project Data      ) *****************************    
    #region " PROJECTS "

    [WebMethod]
    public DataSet Bind_Projects()
    {
        string start_date = "01/01" + DateTime.Now.Year.ToString();
        string end_date = DateTime.Now.ToString("dd/MM/yyyy");
        try
        {
            SQL = " SELECT     PRO.PROJ_CODE,    PRO.PROJ_DESC,    PRO.LONGTUDE,    PRO.LATITUDE,    (SELECT COUNT(*) FROM PS_VISITS VIS WHERE VIS.PROJ_CODE=PRO.PROJ_CODE AND VISIT_DATE BETWEEN '" + start_date + "' AND '" + end_date + "') VIS_CO FROM PS_PROJECTS PRO WHERE PRO.LONGTUDE IS NOT NULL AND TO_NUMBER(PRO.LONGTUDE)>0  AND PRO.LATITUDE IS NOT NULL AND TO_NUMBER( PRO.LATITUDE)>0 ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_Projects >> Fill projects");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_Projects_Chart()
    {       
        try
        {
            SQL = " SELECT     PROJ_DESC,EVAL_VALUE FROM    PS_PROJECTS WHERE    ROWNUM<=10 AND    EVAL_VALUE IS NOT NULL ORDER BY EVAL_VALUE DESC ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_Projects_Chart >> Fill Projects Chart");
        }
        return Ds;
    }

    #endregion    

    //*********************************************************************** ( Reps Data         ) *****************************

    #region " All Reports "

    [WebMethod]
    public DataSet Get_Visits_Rep(string from_date , string to_date  )
    {
        try
        {
            SQL = " SELECT M.CODE AREA_MANG_CODE, SP.CODE AREA_SUPER_CODE ,M.NAME AREA_MANG_NAME , SP.NAME AREA_SUPER_NAME  , SV_REP_STAT(SP.CODE,'" + from_date + "','" + to_date + "','P') PLANNED_SHOPS , SV_REP_STAT(SP.CODE,'" + from_date + "','" + to_date + "','V') VISITED_SHOPS , SV_REP_STAT(SP.CODE,'" + from_date + "','" + to_date + "' ,'VP') VISITED_IN_PLAN , SV_REP_STAT(SP.CODE,'" + from_date + "','" + to_date + "' ,'VU') VISITED_NOT_PLAN  FROM SAL_AREA_MANGERS M , SAL_AREA_SUPERS SP  WHERE SP.MANG_CODE = M.CODE ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Visits_Rep >> Fill Visites Report");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Get_Visits_Trans_Rep(string from_date, string to_date, string user, string supervisor)
    {
        try
        {
            SQL = " SELECT  V.VISIT_NO , V.VISIT_DATE , V.VISIT_TIME  , V.USER_NAME , V.REC_DATE , S.SHOP_NAME , R.NAME as REP_NAME , E.EMP_NAME , T.MESSAGE_TEXT , T.DEPT_REPLAY FROM SV_VISITS V,SV_SHOPS S,SV_REPS R , sv_visit_trans T , sv_emp E WHERE V.SHOP_CODE = S.SAL_CODE      AND V.REP_CODE = R.CODE       AND v.visit_no = t.visit_no      AND T.EMP_NO = E.EMP_NO      AND V.VISIT_DATE BETWEEN '"+from_date+"' AND '"+to_date+"'       AND  REP_CODE IN ( SELECT CODE FROM SV_REPS START WITH CODE = SV_USER_REP('"+user+"')       AND R.CODE in    ( select code from sv_reps where mang_code = '"+supervisor+"' or code = '"+supervisor+"') CONNECT BY PRIOR CODE = MANG_CODE ) ORDER BY V.VISIT_NO   ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Visits_Trans_Rep >> Fill Visites Report");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Get_Visits_Compet_Rep(string from_date, string to_date, string user, string supervisor)
    {
        try
        {
            SQL = " SELECT  V.VISIT_NO , V.VISIT_DATE , V.VISIT_TIME , V.COMPETITORS , V.USER_NAME , V.REC_DATE , V.COMP_TYPE , V.COMP_NOTES ,  S.SHOP_NAME SHOP_NAME, R.NAME REP_NAME FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "' AND  REP_CODE IN( SELECT CODE FROM SV_REPS START WITH CODE = SV_USER_REP('" + user + "') AND R.CODE in ( select code from sv_reps where mang_code = '" + supervisor + "' or code = '" + supervisor + "') CONNECT BY PRIOR CODE = MANG_CODE ) ORDER BY V.VISIT_NO ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Visits_Compet_Rep >> Fill Visites Report");
        }
        return Ds;
    }

    #endregion

    //*********************************

}